<?php

/**
 * @file
 * Kaltura Media Filter module administration functions.
 */

/**
 * Form callback for the Kaltura Media Filter settings form.
 */
function kaltura_media_filter_settings_form($form, &$form_state) {
  $date_format = "Y-m-d H:i:s";

  // This variable is used to let the system know that the settings form was
  // already submitted. Then we are ready to use its configuration.
  $form['kaltura_media_filter_settings_saved'] = array(
    '#type' => 'value',
    '#value' => 1,
  );

  $form['kaltura_media_filter_last_imported'] = array(
    '#title' => t("Import media updated after"),
    '#type' => 'date_popup',
    '#date_timezone' => date_default_timezone(),
    '#date_format' => $date_format,
    '#date_label_position' => 'none',
    '#default_value' => kaltura_media_filter_last_imported($date_format),
    '#required' => TRUE,
    '#description' => t("Only media entries updated after this date will be imported. This date is constantly updated by the module's cron tasks, but you can modify it to reimport some entries or skip some dates.")
  );

  $form['kaltura_media_filter_categories_criteria'] = array(
    '#title' => t("Criteria"),
    '#type' => 'radios',
    '#options' => array(
      'and' => t("Import entries that belong to <strong>ALL</strong> the following categories."),
      'or' => t("Import entries that belong to <strong>ANY</strong> of the following categories."),
    ),
    '#default_value' => variable_get('kaltura_media_filter_categories_criteria', 'and'),
    '#description' => t("This criteria is used if at least one category is selected below.")
  );

  $form['kaltura_media_filter_categories'] = array(
    '#title' => t("Categories"),
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => kaltura_media_filter_categories_options(TRUE),
    '#default_value' => variable_get('kaltura_media_filter_categories', array()),
    '#description' => t("If no categories are selected, all entries will be imported.")
  );

  $form['kaltura_media_filter_import_size'] = array(
    '#title' => t("Import group size"),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array(
      10,
      20,
      30,
      40,
      50,
      60,
      70,
      80,
      90,
      100,
    )),
    '#field_suffix' => ' ' . t("entries"),
    '#default_value' => variable_get('kaltura_media_filter_import_size', 100),
    '#description' => t("This number defines how many entries are grouped to be imported together. Reduce this value if you are having timeout errors."),
  );

  return system_settings_form($form);
}

/**
 * Form callback replacement for the Kaltura import form.
 */
function kaltura_media_filter_entries_import_form($form, &$form_state) {
  // If the module settings aren't configured yet, redirect the user to the
  // settings page.
  if (!variable_get('kaltura_media_filter_settings_saved', 0)) {
    drupal_set_message(t("Before to import entries, you have to set your Media Filter settings in this page."), 'warning');
    drupal_goto('admin/config/media/kaltura/media-filter', array('query' => array('destination' => current_path())));
  }

  try {
    $count = kaltura_media_filter_count_pending();
    $last_imported = kaltura_media_filter_last_imported();

    $form['kaltura_media_filter'] = array(
      '#title' => t("Kaltura Media Filter"),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
    );

    $form['kaltura_media_filter']['last_imported'] = array(
      '#title' => t("Importing from"),
      '#type' => 'item',
      '#markup' => $last_imported ? kaltura_media_filter_last_imported("Y-m-d H:i") : t("Never"),
    );

    $categories_markup = t("<em>No filter</em>");
    $categories_used = kaltura_media_filter_get_categories_used();
    if ($categories_used) {
      if (count($categories_used) > 1) {
        $criteria = variable_get('kaltura_media_filter_categories_criteria', 'and');
        $categories_markup = "Entries with <strong>" . (($criteria == 'and') ? "ALL" : "ANY" ) . "</strong> of the following categories:<ul>";
        foreach ($categories_used as $cat_used) {
          $categories_markup .= '<li>' . $cat_used . '</li>';
        }
        $categories_markup .= '</ul>';
      }
      else {
        $category_used = reset($categories_used);
        $categories_markup = $category_used;
      }
    }

    $form['kaltura_media_filter']['categories'] = array(
      '#title' => t("Categories"),
      '#type' => 'item',
      '#markup' => $categories_markup,
    );

    $form['kaltura_media_filter']['link'] = array(
      '#type' => 'item',
      '#markup' => t("!link to change your filter criteria.", array("!link" => l(t("Click here"), 'admin/config/media/kaltura/media-filter', array('query' => array('destination' => current_path()))))),
    );

    if ($count) {
      $form['count']['#markup'] = format_plural($count, 'There is <strong>1</strong> media entry to be imported.', 'There are <strong>@count</strong> media entries to be imported.');

      $form['actions'] = array('#type' => 'actions');
      $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t("Import"),
      );
    }
    else {
      $form['count']['#markup'] = t('The are no media entries that need to be imported.');
    }
  }
  catch (Exception $e) {
    kaltura_media_filter_report_error('kaltura_media_filter_entries_import_form', $e);
  }

  return $form;
}

/**
 * Form submit callback for kaltura_media_filter_entries_import_form().
 */
function kaltura_media_filter_entries_import_form_submit(&$form, &$form_state) {
  try {
    $queue = DrupalQueue::get('kaltura_media_filter_bulk_update_cron');
    $queue->deleteQueue();

    $batch = array(
      'operations' => array(
        array('kaltura_media_filter_batch_import', array()),
      ),
      'title' => t('Importing Kaltura Media Entries (using Kaltura Media Filter)'),
      'init_message' => t('Importing...'),
      'progress_message' => t('@estimate remained.'),
      'finished' => 'kaltura_media_filter_batch_import_finished',
      'file' => drupal_get_path('module', 'kaltura_media_filter') . '/kaltura_media_filter.admin.inc',
    );

    batch_set($batch);
  }
  catch (Exception $e) {
    kaltura_media_filter_report_error('kaltura_media_filter_entries_import_form_submit', $e);
  }
}

/**
 * Batch callback used to import Kaltura entries.
 */
function kaltura_media_filter_batch_import(&$context) {
  variable_set('kaltura_media_filter_last_batch_import', time());

  $first_run = FALSE;
  if (empty($context['sandbox'])) {
    $first_run = TRUE;
    $context['sandbox'] = array(
      'start' => TRUE,
    );
  }

  if ($first_run) {
    $context['results']['total'] = kaltura_media_filter_count_pending();
    $context['results']['imported'] = kaltura_media_filter_bulk_import(0);
    $context['finished'] = 0;
  }
  else {
    $total = &$context['results']['total'];
    $imported = &$context['results']['imported'];
    $max_time = &$context['results']['max_time'];
    $page = &$context['sandbox']['page'];
    $finished = &$context['finished'];

    $imported_this_run = kaltura_media_filter_bulk_import();
    $imported += $imported_this_run;
    $imported = min($imported, $total);
    $finished = min(0.999999, $imported / $total);
    if (empty($imported_this_run)) {
      $finished = 1;
    }
  }

  $context['message'] = t("@count of @total already imported.", array('@count' => $context['results']['imported'], '@total' => $context['results']['total']));
}

/**
 * Batch finished callback used to update the variable that stores the last
 * updated entry.
 */
function kaltura_media_filter_batch_import_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(format_plural($results['imported'], "1 entry imported.", "@count entries imported."));
  }
  else {
    drupal_set_message(t("Finished with an error."), 'error');
  }
  variable_del('kaltura_media_filter_last_batch_import');
}
