Kaltura Media Filter

This module overrides Kaltura cron and batch import functions to let you filter
the entries imported by date and categories.
